/**
* Copyright (c) 2006-2012 LOVE Development Team
*
* This software is provided 'as-is', without any express or implied
* warranty.  In no event will the authors be held liable for any damages
* arising from the use of this software.
*
* Permission is granted to anyone to use this software for any purpose,
* including commercial applications, and to alter it and redistribute it
* freely, subject to the following restrictions:
*
* 1. The origin of this software must not be misrepresented; you must not
*    claim that you wrote the original software. If you use this software
*    in a product, an acknowledgment in the product documentation would be
*    appreciated but is not required.
* 2. Altered source versions must be plainly marked as such, and must not be
*    misrepresented as being the original software.
* 3. This notice may not be removed or altered from any source distribution.
**/

#ifndef LOVE_JOYSTICK_XINPUT_JOYSTICK_H
#define LOVE_JOYSTICK_XINPUT_JOYSTICK_H

// LOVE
#include <joystick/Joystick.h>

// XINPUT
#include <windows.h>
#include <Xinput.h>

/* FUNCTIONALITY NOTES

Since Xinput reported xbox devices have various capabilities, in order to keep them straight across all devices, 
we make sure that indexes for axes and buttons are consistent.

Since the triggers can be either fully analog or digital, both are reported handily in the same index.



Axes
0: Left X
1: Left Y
2: Right X
3: Right Y
4: Left Trigger
5: Right Trigger

Buttons
0: A
1: B
2: X
3: Y
4: Left Trigger
5: Right Trigger
6: LB
7: RB
8: LS
9: RS
10: Back
11: Start



*/


namespace love
{
namespace joystick
{
namespace xinput
{
#define MAXCONTROLLERS 4
	class Joystick : public love::joystick::Joystick
	{
	private:
		bool padValid[MAXCONTROLLERS];

		unsigned char triggerPressThreshold; 

		XINPUT_STATE currentState[MAXCONTROLLERS];
		XINPUT_STATE oldState[MAXCONTROLLERS];

		XINPUT_CAPABILITIES caps[MAXCONTROLLERS];

	public:
		Joystick();
		virtual ~Joystick();

		float clampval(float x);
		bool wasButtonDown(int index, int button);
		bool getButtonDown(int index, int button);
		void notifyRelease(lua_State *L, int index, int button);
		void notifyPress(lua_State *L, int index, int button);

		// Implements Module.
		const char * getName() const;

		void update(lua_State *L);
		void setTriggerThreshold(int threshold) { triggerPressThreshold = threshold; }
		int getNumJoysticks();
		const char * getName(int index);
		bool open(int index);
		bool isOpen(int index);
		int getNumAxes(int index);
		int getNumBalls(int index);
		int getNumButtons(int index);
		int getNumHats(int index);
		float getAxis(int index, int axis);
		int getAxes(lua_State * L);
		int getBall(lua_State * L);
		bool isDown(int index, int * buttonlist);
		Hat getHat(int index, int hat);
		void close(int index);

	private:

	}; // Joystick

} // xinput
} // joystick
} // love

#endif // LOVE_JOYSTICK_XINPUT_JOYSTICK_H
