Xinput For Love2d
=================

This is a drop in module replacement for Love's joystick module, which uses Xinput instead of SDL, in order to facilitate proper Xbox 360 controller support. You can either integrate the code yourself and build a love2d executable, or used the prepackaged executable as a drop in replacement. 

Usage
------------


###How To Build
First, figure out how to build Love2d from scratch. [This](https://love2d.org/wiki/Building_L%C3%96VE) is a good reference.

Second, browse to `src/modules/joystick/sdl`, and simply replace the four files in there with the files from this repository. It should build without issues. 

Alternately, so you can switch between them, what you can do instead is make an xinput directory that sits next to the SDL directory, and then modify the project file so that the new files are included. If you try and build like this you will get conflicts because they both define the same things, so flag the SDL version to not compile.

Note: You need the [Direct X SDK (June 2010)](http://www.microsoft.com/en-us/download/details.aspx?id=6812) installed with paths correctly set in order to build Xinput for Love2d. It should go without saying that Xinput for Love2d is Windows specific.

###Lua Reference

Xinput for Love2d supports all existing Love2d Joystick Module functions found [here](https://love2d.org/wiki/love.joystick).

#### Button and Axis IDs

Xinput for Love2d stores button and axis data at the same indexes regardless of connected device. Future versions may support device specific id labels and interfaces provided by an external lua library. 

##### Buttons
1. A
2. B
3. X
4. Y
5. Left Trigger
6. Right Trigger
7. LB
8. RB
9. LS
10. RS
11. Back
12. Start

##### Axes
1. Left X
2. Left Y
3. Right X
4. Right Y
5. Left Trigger
6. Right Trigger

#### New Functions
`love.joystick.update()`

This function must be called at the beginning of `love.update()`. Because XInput is built around a polling system rather than an event system, this gets a new controller state, and fires off all state changes to love.joystickpressed() and love.joystickreleased()

`love.joystick.setTriggerThreshold(threshold)`

threshold: 1-255

Because the triggers are axes but can be used as buttons, Xinput for Love2d reports them as both types of inputs. This threshold is the axis value at which it will consider the trigger as pressed or released.

Notes
-----

I've tested this with a GH2 Guitar and a normal wired Xbox 360 Controller. Both work great. You can also test with a testing app I've thrown together [here](https://github.com/mrcharles/love2d-x360-test)

For now, any 360 device connected reports 6 axes and 12 buttons. Not all of them will be functional, depending on your device. You can get an idea of what *will* work [here](http://msdn.microsoft.com/en-us/library/windows/desktop/hh405050(v=vs.85).aspx).

Xinput devices do not need to be 'opened', a paradigm which is being removed in Love 0.8.1. The open(), close(), and isOpen() functions exist but are mostly useless.

Because of reporting difference between SDL and Xinput, any existing controller code you have will be messed up. Axes specifically will be way wrong. I considered changing Xinput for Love2d to match the SDL, but decided it was best to leave the input as close to raw as possible so that people used to Xbox360 development or people who've used Xinput before will see values consistent with what they are used to.

I've started a thread on [Love2d.org](https://love2d.org/forums/viewtopic.php?f=4&t=10356) where I talk about the development of this project. Feel free to join in.

Coming Soon
-----------

* Vibration Support
* Deadzone support
* Lua library for higher level controller management
* D-Pad smoothing to combat terrible xbox 360 dpad issues.
* Stick transformation libraries. 